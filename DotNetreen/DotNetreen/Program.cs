﻿using System;

namespace DotNetreen
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: Dotnetreen <filename>");
                return;
            }

            // Start Coding!
            new DotNetreen(args[0]).Run();
        }
    }
}
