﻿using System;

namespace DotNetreen
{
    public class Alarm
    {
        // Alarm will be off at startup
        bool isOn = false;

        public string Name { get; set; }
        public byte DataStreameId { get; set; }
        public int OnCondition { get; set; }
        public int OffCondition { get; set; }
        public bool IsOn
        {
            get
            {
                return isOn;
            }
            set
            {
                isOn = value;
            }
        }
        public bool IsOnGreaterThanOff
        {
            get
            {
                return OnCondition > OffCondition;
            }
        }

        public Alarm(string row)
        {
            var line = row.Split(' ');

            Name = line[0];
            DataStreameId = Convert.ToByte(line[1]);
            OnCondition = Convert.ToInt32(line[2].Split('=')[1]);
            OffCondition = Convert.ToInt32(line[3].Split('=')[1]);
        }
    }
}
