﻿using System.Collections.Generic;

namespace DotNetreen
{
    public class DataStreamElement
    {
        public byte DataStreameId { get; set; }
        public KeyValuePair<uint, int> Measurement { get; set; }

        public DataStreamElement()
        {
        }

        public DataStreamElement(byte dataStreameId, KeyValuePair<uint, int> measurement)
        {
            DataStreameId = dataStreameId;
            Measurement = measurement;
        }
    }
}
