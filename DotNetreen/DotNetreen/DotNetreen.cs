﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace DotNetreen
{
    public class DotNetreen
    {
        bool isValidInputFile;
        List<Alarm> lstAlarm = new List<Alarm>();
        List<DataStream> lstDataStream = new List<DataStream>();

        public DotNetreen(string file)
        {
            isValidInputFile = InitDotNetreen(file);
        }

        public void Run()
        {
            if (!IsReadyToRun())
                return;

            while (lstDataStream.Count > 0)
            {
                // Fetch the next data element from stream
                var dataStreamElement = FindNextDataStreamElement();

                // Process data stream by matching Alarms
                ProcessDataStream(dataStreamElement);

                // Remove this data element from stream once processed
                lstDataStream = RemoveFromDataStream(dataStreamElement);
            }
        }

        private bool InitDotNetreen(string inputFile)
        {
            string row;

            try
            {
                using (var reader = new StreamReader($"{Directory.GetCurrentDirectory()}/{inputFile}"))
                {
                    if (reader.EndOfStream)
                    {
                        Console.WriteLine("DotNetreen input file is empty!");
                        return false;
                    }

                    while ((row = reader.ReadLine()) != null)
                    {
                        if (row[0] == '-') break;
                        lstAlarm.Add(new Alarm(row));
                    }

                    while ((row = reader.ReadLine()) != null)
                    {
                        lstDataStream.Add(new DataStream(row));
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something went wrong while reading the input file! {ex.Message}");
                return false;
            }
        }

        private bool IsReadyToRun()
        {
            if (!isValidInputFile)
            {
                return false;
            }

            if (lstAlarm.Count == 0)
            {
                Console.WriteLine("DotNetreen have no Alarms configured!");
                return false;
            }

            if (lstDataStream.Count == 0)
            {
                Console.WriteLine("DotNetreen have no DataStream!");
                return false;
            }

            return true;
        }

        private void ProcessDataStream(DataStreamElement data)
        {
            var alarms = lstAlarm.Where(x => x.DataStreameId == data.DataStreameId);

            foreach (var alarm in alarms)
            {
                if (IsAlarmTriggered(data.Measurement.Value, alarm))
                {
                    // Toggle alarm status on trigger
                    alarm.IsOn = !alarm.IsOn;

                    // Display the message
                    Console.WriteLine($"[{data.Measurement.Key}] {alarm.Name} {(alarm.IsOn ? "ON" : "OFF")} WAS {data.Measurement.Value}");
                }
            }
        }

        private bool IsAlarmTriggered(int dataValue, Alarm alarm)
        {
            bool isTriggered = false;

            if (alarm.IsOnGreaterThanOff)
            {
                if (dataValue >= alarm.OnCondition)
                {
                    isTriggered = !alarm.IsOn;
                }
                else if (dataValue <= alarm.OffCondition)
                {
                    isTriggered = alarm.IsOn;
                }
            }
            else
            {
                if (dataValue <= alarm.OnCondition)
                {
                    isTriggered = !alarm.IsOn;
                }
                else if (dataValue >= alarm.OffCondition)
                {
                    isTriggered = alarm.IsOn;
                }
            }

            return isTriggered;
        }

        private DataStreamElement FindNextDataStreamElement()
        {
            if (lstDataStream.Count == 0) return null;

            DataStreamElement smallest = new DataStreamElement(lstDataStream[lstDataStream.Count-1].DataStreameId, lstDataStream[lstDataStream.Count - 1].Measurements[0]);

            for (int i = lstDataStream.Count; i > 0; i--)
            {
                DataStreamElement next = new DataStreamElement(lstDataStream[i-1].DataStreameId, lstDataStream[i-1].Measurements[0]);

                if (next.Measurement.Key < smallest.Measurement.Key)
                {
                    smallest = next;
                }
                else if (next.Measurement.Key == smallest.Measurement.Key)
                {
                    if (next.DataStreameId < smallest.DataStreameId)
                    {
                        smallest = next;
                    }
                }
            }

            return smallest;
        }

        private List<DataStream> RemoveFromDataStream(DataStreamElement dataStreamElement)
        {
            // Remove element from the DataStream
            var lst = lstDataStream.Where(x => x.DataStreameId == dataStreamElement.DataStreameId).FirstOrDefault();
            var elementToRemove = lst.Measurements.Find((lItem) => lItem.Equals(dataStreamElement.Measurement));
            lst.Measurements.Remove(elementToRemove);

            // Remove DataStream item when its Measurements are empty
            if (lst.Measurements.Count == 0)
            {
                lstDataStream.Remove(lst);
            }

            return lstDataStream;
        }
    }
}
