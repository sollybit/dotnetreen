﻿using System;
using System.Collections.Generic;

namespace DotNetreen
{
    public class DataStream
    {
        public byte DataStreameId { get; set; }
        public List<KeyValuePair<uint, int>> Measurements { get; set; }

        public DataStream(string row)
        {
            var line = row.Split(' ');
            string[] pair = null;
            KeyValuePair<uint, int> keyValuePair;

            DataStreameId = Convert.ToByte(line[0].Trim(new char[] { '[', ']' }));

            Measurements = new List<KeyValuePair<uint, int>>(line.Length-1);

            for (int i = 1; i < line.Length; i++)
            {
                pair = line[i].Split(':');
                keyValuePair = new KeyValuePair<uint, int>(Convert.ToUInt32(pair[0]), Convert.ToInt32(pair[1]));
                Measurements.Add(keyValuePair);
            }
        }
    }
}
