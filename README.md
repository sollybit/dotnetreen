#Programming Problem

This is OutpostCentrals programming task for C# developers.  	

#Problem

Here at OutpostCentral we deal with a lot of time series data, and one thing our devices can do is trigger alarms on various conditions on that data.  The problem we'd like you to solve is, given a definition of alarms and some time series data, write out a time ordered sequence of alarm activations ( if any ).

An alarm is triggered when the data reaches a given ON level, and turns off again when the data goes below an OFF level.  The alarm can't trigger ON a 2nd time until it has triggered OFF and vice versa.

An alarm is for a specified data stream. Multiple alarms can be configured for a data stream.

If the ON level is greater than the OFF level, then the trigger happens when it goes above the ON level

If the ON level is less than the OFF level, then the trigger happens when the data goes below the ON level

If there are multiple activations at the same time then the output should show in numeric order of the datastream identifier


#Our Expectation

- Code that you'd normally produce as part of your job
- Your code will be a point of discussion in an interview
- You are free to ask any questions to clarify what you need to do
- Above the main function, you can write any notes / assumptions or anything else we might need to know when looking at your solution


#Enviroment


You will need the latest version of Visual Studio 2017 with C# / .NET installed

This repository has a visual studio console project pre setup for C#

The default project setup in visual studio uses input.txt as the default arguments to the program.  The definition in input.txt is just an example and not meant to test all possible edge cases

You are free to add what you like to the project.  However it must be compilable as a fresh checkout from Git.

You can setup your own bitbucket account and fork the code, or you can download the repository from https://bitbucket.org/outpostcentral/dotnetreen/downloads/ 

You can then either send us a invite/link to your repository or send us a zip with your final code

#Details

We will test the code by giving an input file to your program and checking the output to stdout.  

The input file will follow the following format :-

all lines will use windows line endings \r\n

It will be divided into two halves

    <ALARMS>
    -
    <DATA>

with the following definitions:

    <ALARMS> ::= ""|<ALARM>\r\n<ALARMS>
    <ALARM> ::= <NAME><SPACE><DATASTREAM><SPACE><ONCONDITION><SPACE><OFFCONDITION>
    <NAME> ::= <UP TO 8 CHAR>
    <DATASTREAM> ::= <BYTE>
    <ONCONDITION> ::= "ON="<INT>
    <OFFCONDITION> ::= "OFF="<INT>
    <DATA> ::= ""|<DATASTREAMID><SPACE><DATAPOINTS>\r\n<DATA>
    <DATASTREAMID> ::= "["<DATASTREAM>"]"
    <DATAPOINTS> ::= "" | <MEASUREMENT><SPACE><DATAPOINTS>
    <MEASUREMENT> ::= <TIME>":"<VALUE>
    <TIME> ::= <UNSIGNED INT>
    <VALUE> ::= <INT>
    <SPACE> ::= " "



The output should look like

    [<time>] <NAME> <ON | OFF> WAS <ACTUAL VALUE THAT TRIGGERED THE ON/OFF LEVEL>

#Example File 

    A 1 ON=10 OFF=8
    B 1 ON=0 OFF=1
    C 2 ON=5 OFF=4
    -
    [1] 100:0 105:1 106:10 110:9 115:8 120:-1 130:0 135:-4 140:5
    [2] 100:0 101:1 116:7 117:1 118:4 119:8


#Output ( Sent to Console )

    [100] B ON WAS 0
    [105] B OFF WAS 1
    [106] A ON WAS 10
    [115] A OFF WAS 8
    [116] C ON WAS 7
    [117] C OFF WAS 1
    [119] C ON WAS 8
    [120] B ON WAS -1
    [140] B OFF WAS 5
    

